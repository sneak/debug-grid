(function setupDebugGrid() {
	// Set values for the grid
	const cols = 12
	const width = 72 // Inner width of column
	const gutter = 40 // Combined left & right gutter total
	const pageGutter = 24 // Amount of margin to add each side of the grid container
	
	// Some variables for later use
	let debugGridActive = false
	let debugGuttersActive = false
	const grid = document.createElement('div')
	grid.classList.add('debug-grid')
	const gridWithGutters = document.createElement('div')
	gridWithGutters.classList.add('debug-grid')
	
	function generateGridMarkup() {
		const col = document.createElement('div')
		const max = cols * (width + gutter)
		let gridStyles = 'display: flex; z-index: 9999; position: fixed; top: 0; bottom: 0; left: 0; right: 0; margin-left: auto; margin-right: auto; pointer-events: none; opacity: 0.05; box-sizing: content-box !important;'
		gridStyles += ' max-width: ' + max + 'px;'
		if (pageGutter > 0) {
			gridStyles += ' padding-left: ' + pageGutter + 'px; padding-right: ' + pageGutter + 'px;'
		}
		let colStyles = 'flex: 1; background-color: #f00; margin-left: ' + (gutter / 2) + 'px; margin-right: ' + (gutter / 2) + 'px;'
		grid.setAttribute('style', gridStyles); col.setAttribute('style', colStyles)
		for (let i = 0; i < cols; i++) {
			const clone = col.cloneNode(true)
			grid.appendChild(clone)
		}
	}

	function generateGridMarkupWithGutters() {
		const col = document.createElement('div')
		const max = cols * (width + gutter)
		let gridStyles = 'display: flex; z-index: 9999; position: fixed; top: 0; bottom: 0; left: 0; right: 0; margin-left: auto; margin-right: auto; pointer-events: none; opacity: 0.1; box-sizing: content-box !important;'
		gridStyles += ' max-width: ' + max + 'px;'
		if (pageGutter > 0) {
			gridStyles += ' padding-left: ' + pageGutter + 'px; padding-right: ' + pageGutter + 'px;'
		}
		let colStyles = 'flex: 1; display: flex; justify-content: space-between;'
		gridWithGutters.setAttribute('style', gridStyles)
		col.setAttribute('style', colStyles)
		for (let i = 0; i < cols; i++) {
			const clone = col.cloneNode(true)
			const gutterRight = document.createElement('div')
			const gutterLeft = document.createElement('div')
			gutterRight.style.backgroundColor = '#f00'
			gutterRight.style.width = (gutter / 2) + 'px'
			gutterLeft.style.backgroundColor = '#00f'
			gutterLeft.style.width = (gutter / 2) + 'px'
			clone.appendChild(gutterRight)
			clone.appendChild(gutterLeft)
			gridWithGutters.appendChild(clone)
		}
	}
	
	function showDebugGrid() {
		if (debugGuttersActive) {
			hideDebugGutters()
		}
		document.body.appendChild(grid)
		debugGridActive = true
	}
	
	function hideDebugGrid() {
		document.body.removeChild(grid)
		debugGridActive = false
	}
	
	function showDebugGutters() {
		if (debugGridActive) {
			hideDebugGrid()
		}
		document.body.appendChild(gridWithGutters)
		debugGuttersActive = true
	}
	
	function hideDebugGutters() {
		document.body.removeChild(gridWithGutters)
		debugGuttersActive = false
	}
	
	document.addEventListener('keydown', function(zEvent) {
		if (zEvent.ctrlKey && zEvent.altKey && zEvent.shiftKey && zEvent.key === '/') {
			if (debugGuttersActive) {
				hideDebugGutters()
			} else {
				showDebugGutters()
			}
		} else if (zEvent.ctrlKey && zEvent.altKey && zEvent.key === '/') {
			if (debugGridActive) {
				hideDebugGrid()
			} else {
				showDebugGrid()
			}
		}
	})
	
	generateGridMarkup()
	generateGridMarkupWithGutters()

	// Check for querystring parameter 'grid' or 'gutters'
	const queryString = window.location.search
	const urlParams = new URLSearchParams(queryString)
	const gridParam = urlParams.has('columns')
	const guttersParam = urlParams.has('gutters')
	
	window.onload = function() { // Don't call before dom is ready
		if (gridParam) {
			showDebugGrid()
		} else if (guttersParam) {
			showDebugGutters()
		}
	}
})()