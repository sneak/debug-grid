# debug-grid

A javascript tool for adding & removing a grid overlay to help while working on layouts

## Installation

Include the `debug-grid.js` file in your project. It will automatically initialise when loaded.

## Configuration

To configure the grid settings for your site, open the `debug-grid.js` file and edit the `cols`, `width` and `gutter` values at the top of the file.

## Toggling the grid

Once loaded in your website you can use the following key combos to show the grid overlay:

`Control + Option + /` — Shows the base columns

`Control + Option + Shift + /` — Shows the column gutters only

The grid can be hidden again by using the same key combos.

## Using a querystring parameter

If you want to continue showing the overlay between refreshes without having to use the key commands again, you can add a querystring parameter to your site URL:

`?columns` will show the columns

`?gutters` will show the column gutters

If you are chaining querystring values and need to supply a value for the parameter, you can enter anything you like — the value is ignored (e.g. `&columns=yes&othervalue=X`)

## Additional styles

A class of `debug-grid` is added to the overlay so that you can target it for any further CSS rules (e.g. adding page gutters outside of the grid width, forcing column size changes at mediaquery breakpoints)